import React, { Component } from 'react';
import './App.css';
import TableControl from './containers/TableControl/TableControl';

class App extends Component {

  render() {
    return (
      <TableControl />
    );
  }
}

export default App;
