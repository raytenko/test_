import React, { Component } from 'react';
import Table from '../../components/table/table';

import { transformData } from './transformData';
import { getJson } from './api';

class TableControl extends Component {
    state = {
        tableData: null,
        showOnlyOpen: false,
        pending: true,
        error: false
    }

    filterHandler = (event) => {
        this.setState({
            showOnlyOpen: event.target.checked
        })
    }

    componentDidMount() {
        this.setState({
            pending: true
        })
        // API without open
        // getJson(`https://api.myjson.com/bins/1emyo2`)  
        
        getJson('https://api.myjson.com/bins/17mh5u')
            .then((data) => {
                this.setState({
                    tableData: transformData(data)
                })
            })
            .catch(error => {
                console.log(error);
                this.setState({
                    error: true
                })
            })
            .finally(() => {
                this.setState({
                    pending: false
                })
            });
    }

    render() {
        let table;
        if (this.state.pending) {
            table = <div className='preloader'>Loading</div>
        }
        if (this.state.tableData) {
            table = <Table
                data={this.state.tableData}
                filterHandler={this.filterHandler}
                showOnlyOpen={this.state.showOnlyOpen} />
        }
        if (this.state.error) {
            table = <div>Error</div>
        }

        return (
            <React.Fragment>
                {table}
            </React.Fragment>
        )
    }
}

export default TableControl;