function isOpen(elemArray) {
    const uTime = Date.now();
    for (const obj of elemArray) {
        if (obj.from < uTime && uTime < obj.to) {
            return true
        }
    }
    return false;
}

export function transformData(data) {
    return data.map(elem => {
        return {
            ...elem,
            isOpen: isOpen(elem.tradingHours)
        }
    })
}

