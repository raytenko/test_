import React from 'react';
import TableRow from '../tableRow/tableRow';
import PropTypes from 'prop-types';

import './table.css';

const table = ({ data, showOnlyOpen, filterHandler }) => {
    let tableData;
    tableData = data.map(row => {
        return <TableRow key={row.instrumentID} dataRow={row} />
    });

    if (showOnlyOpen) {
        tableData = data.filter(elem => elem.isOpen).map(row => {
            return <TableRow key={row.instrumentID} dataRow={row} />
        });
    }

    let tableMarkup;
    tableMarkup = (
        <table className='table'>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Is open</th>
                </tr>
            </thead>
            <tbody>
                {tableData}
            </tbody>
        </table>
    )

    if (tableData.length === 0) {
        tableMarkup = <div>All are closed</div>;
    }

    return (
        <div className='wrapper'>
            {tableMarkup}
            <div className='table-filter'>
                <label htmlFor='filter'>Show only open</label>
                <div>
                <input id='filter' type="checkbox" name='showOnlyOpen' checked={showOnlyOpen} onChange={filterHandler} />
                </div>
                
            </div>
        </div>
    )
}

table.propTypes = {
    data: PropTypes.array,
    showOnlyOpen: PropTypes.bool,
    filterHandler: PropTypes.func
}

export default table;

