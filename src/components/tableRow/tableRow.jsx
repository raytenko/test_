import React from 'react';
import './tableRow.css';

import PropTypes from 'prop-types';

const tableRow = ({ dataRow }) => {

    const isOpen = dataRow.isOpen ? 'Open' : 'Closed';
    
    return (
        <tr className='table-row'>
            <td className='table-row-item'>{dataRow.instrumentID}</td>
            <td className='table-row-item'>{dataRow.name}</td>
            <td className='table-row-item'>{isOpen}</td>
        </tr>
    )
}

tableRow.propTypes = {
    dataRow: PropTypes.object,
}

export default tableRow;

